﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public int NumberOfMonsters;
    public int Increment;
    public GameObject MonsterToSpawn;
    public SetText TextUI;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(WaveStart());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpawnWave()
    {
        for (int x = 0; x < NumberOfMonsters; x++)
        {
            GameObject storedMonster = Instantiate(MonsterToSpawn);
            storedMonster.gameObject.SetActive(true);
    
        }
    }
        IEnumerator WaveStart()
        {
            while (true)
            {
           
                SpawnWave();
            NumberOfMonsters += Increment;
            TextUI.WaveNumber += 0.5f;
            yield return new WaitForSeconds(10);
        }
        }
    }

