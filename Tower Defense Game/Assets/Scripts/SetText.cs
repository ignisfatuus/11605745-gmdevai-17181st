﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SetText : MonoBehaviour {

    public Text WaveText;
    public float WaveNumber;
	// Use this for initialization
	void Start ()
    {
        WaveText = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        WaveText.text = "Wave " + WaveNumber;
	}


}
