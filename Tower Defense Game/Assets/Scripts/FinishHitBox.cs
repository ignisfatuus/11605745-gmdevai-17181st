﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FinishHitBox : MonoBehaviour {

    public SetLifeText LifeText;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
        
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Monster")
        {
            LifeText.LifeNumber -= 1;
            Debug.Log("Finish");
            collision.gameObject.SetActive(false);
        }
    }
}
