﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SetLifeText : MonoBehaviour {

    public Text WaveText;
    public float LifeNumber=3;
    public Text GameOverText;
    public Button RetryButton;
    // Use this for initialization
    void Start () {
        WaveText = gameObject.GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        CapLifeNumber();
        WaveText.text = "Life " + LifeNumber;
    }

    void CapLifeNumber()
    {
        if(LifeNumber <1 )
        {
            GameOverText.gameObject.SetActive(true);
            RetryButton.gameObject.SetActive(true);
            Time.timeScale = 0;
            LifeNumber = 0;
        }
    }
}
